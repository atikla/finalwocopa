import React, { memo} from 'react'; 
import { View , Text ,Image ,StyleSheet , ImageBackground,ActivityIndicator,Platform } from 'react-native'; 
import { Icon } from 'react-native-elements' 
import {
    responsiveHeight,
    responsiveWidth,
    responsiveScreenFontSize
  } from "react-native-responsive-dimensions"; 
import { useHistory } from 'react-router-native'; 
import ImagePicker from 'react-native-image-picker';    
import {ApiUpdateAvatar} from '../../services/api'
import {requestMultiple, PERMISSIONS} from 'react-native-permissions';
var RNFS = require('react-native-fs');

const UpperSection = ({...props}) => {  
 
    let history = useHistory();
    let ProfileData = props.ProfileData
    let avater = props.avater

 
    const options = {
      title: 'Select Avatar',
      customButtons: [{ name: 'fb', title: 'Choose Photo from Facebook' }],
      storageOptions: {
        skipBackup: true,
        path: 'images', 
      },
    };
 
    const trimavater =(avater)=> {
        try{  
            if (avater.startsWith("http://app.tech-solt.com/")) {
              return avater 
            }
            else{
              let Newavater = "http://app.tech-solt.com/"+'/' + avater
              return Newavater
            }
          }
        catch(err){ 
        } 
      } 
     
 
      const _changeName =()=>{ 
        history.push({pathname : '/Main/Profile/Name',
        state: {ProfileData}
        })   
      }
  
      const _changeWork = () => { 
        history.push({pathname : '/Main/Profile/Work',
        state: {ProfileData}
        })    
      } 

      const askfor = () =>{
        requestMultiple([PERMISSIONS.IOS.CAMERA, PERMISSIONS.IOS.PHOTO_LIBRARY]).then(
          (statuses) => {
            console.log('Camera', statuses[PERMISSIONS.IOS.CAMERA]);
            console.log('MEDIA_LIBRARY', statuses[PERMISSIONS.IOS.PHOTO_LIBRARY]);
            if (statuses[PERMISSIONS.IOS.CAMERA] ==='granted' && statuses[PERMISSIONS.IOS.PHOTO_LIBRARY]==='granted'){
               _pickImage()
            }
          },
        ).catch(err =>{ 
        })
      }
 

      const _pickImage = async () =>{ 
          ImagePicker.showImagePicker(options, (response) => {  
          if (response.didCancel) {
            // console.log('User cancelled image picker');
          } else if (response.error) {
            // console.log('ImagePicker Error: ', response.error);
          } else if (response.customButton) {
            // console.log('User tapped custom button: ', response.customButton);
          } else { 
            props.showInidcator()
            let bodyFormData = new FormData();  
            bodyFormData.append('access_token',global.AccesToken)
            bodyFormData.append('avater', {
              uri: response.uri,
              type: 'image/jpeg',
              name: response.uri.substring(response.uri.lastIndexOf('/')+1)
            }); 
            ApiUpdateAvatar(bodyFormData).then((response) => { 
              props.setAvater(response.data.avater) 
              // ToastAndroid.show('Data Has been Changed!', ToastAndroid.SHORT); 
              props.closeInidcator() 
            })
            .catch(err => {
              props.closeInidcator() 
              return;
            }); 
          
            // var files = [
            //   {
            //     name: 'files',
            //     filename: Platform.OS === 'android' ?   response.fileName : response.uri.substring(response.uri.lastIndexOf('/')+1), 
            //     filepath:  Platform.OS === 'android' ?  response.path : response.uri,
            //     filetype:response.type
            //   }, 
            // ];
            // console.log('image',files)
            // var uploadUrl = 'http://app.tech-solt.com/endpoint/v1/27ce3f02d98d388b9021021f74ffe702/users/update_avater_new';
 
            //   RNFS.uploadFiles({
            //     toUrl: uploadUrl,
            //     files: files,
            //     method: 'POST',
            //     headers: {
            //       'Accept': 'application/json',
            //     },
            //     fields: {
            //       'access_token': global.AccesToken , 
            //     },
            //     begin: uploadBegin,
            //     progress: uploadProgress
            //   }).promise.then((response) => { 
            //     let avater =  JSON.parse(response.body).avater 
            //       if (response.statusCode == 200) {
            //         console.log(response.statusCode)
            //         props.setAvater(avater) 
            //         props.closeInidcator() // response.statusCode, response.headers, response.body
            //       } else {
            //         // console.log('SERVER ERROR');
            //       }
            //     })
            //     .catch((err) => {
            //       if(err.description === "cancelled") {
            //         // cancelled by user
            //       }
            //       // 
            //       props.closeInidcator() // response.statusCode, response.headers, response.body

            //     });
                  
            //     }
            //   }); 
          }
        })
      }
  

        var uploadBegin = (response) => {
          props.showInidcator()
          var jobId = response.jobId; 
        };
         
        var uploadProgress = (response) => {  
          var percentage = Math.floor((response.totalBytesSent/response.totalBytesExpectedToSend) * 100); 
        };
         
 
     

    return (  
        <View style={styles.header}>   

        
                <ImageBackground imageStyle={{opacity:0.8 }} source={{uri: trimavater(avater)}} style={{width: '100%', height: responsiveHeight(40) ,justifyContent:'center'}}>
                 
                    <View  style = {{backgroundColor: 'rgba(0, 15, 54 , 0.7)' ,width: '100%', height: '100%' , alignItems: 'center',padding:10 }} >

                      <View style={{flexDirection:'row',width:'100%',justifyContent:'space-between'  }}>

                        <Icon name="arrow-back" type ={'material'} size={responsiveScreenFontSize(3.5)}   iconStyle={{color:'#e6e6e6'}} onPress={ ()=>history.goBack()} />
                        
                        <Icon name="cog" type ={'font-awesome'} size={responsiveScreenFontSize(3.5)}    iconStyle={{color:'#e6e6e6'}}   onPress={()=> history.push('/Main/Profile/Settings')} />
 
                      </View>
                    
                      
                      <View style={styles.headerContent}> 

                          <View  style={{flexDirection:'row'}}>
                              <Image style={styles.avatar} source={{uri: trimavater(avater)}}/> 
                              <Icon
                                      name='edit'
                                      type='font-awesome'
                                      color='#F16731'
                                      containerStyle={{  zIndex:1, alignSelf:'center',marginStart:-5}} 
                                      size={responsiveScreenFontSize(2)}
                                      onPress ={  askfor}
                                  />  
                          </View> 

                          <View style={{height:responsiveHeight(0.1)}}>
                             <ActivityIndicator size="large" color="#ff4d00" animating={props.showIndicator} />
                          </View>

                          <View style={styles.lineEdit}>
                            < Text style={styles.nameNew}>{(ProfileData.first_name && ProfileData.last_name)? ProfileData.first_name + " " + ProfileData.last_name : "Unknown"}</Text> 
                              <Icon
                                  name='edit'
                                  type='font-awesome'
                                  color='#F16731'
                                  containerStyle={{ marginStart : 3,marginTop : 3}} 
                                  onPress ={_changeName}
                                  size={responsiveScreenFontSize(2)}
                                 
                              /> 
                          </View>

                          <View style={styles.lineEdit}>

                            <Text style={styles.userInfoNew}>{(ProfileData.worktitle_text && ProfileData.worktitle_address  ) ? ProfileData.worktitle_text + " at " + ProfileData.worktitle_address : "Unknown"}</Text>
                            <Icon
                                name='edit'
                                type='font-awesome'
                                color='#F16731'
                                containerStyle={{ marginStart : 3,marginTop : 1}} 
                                onPress ={_changeWork}
                                size={responsiveScreenFontSize(2)}
                            /> 

                          </View> 

                      </View>

                  </View>

              </ImageBackground>

            </View>
        )
} 

export default memo(UpperSection);

const styles = StyleSheet.create({   

  header:{
    backgroundColor: "#ffffff",
    alignItems: 'center', 
  },
  headerContent:{
    alignItems: 'center', 
  },
  backImage: {
    alignSelf: 'stretch',
    height: responsiveHeight(35),   
   
  },
  userInfoNew:{
    marginStart : 16,
    fontSize:responsiveScreenFontSize(2),
    color:"#ffffff",
    fontWeight:'200',
    fontFamily:'EBGaramond-Medium'
  },
  lineEdit:{
    flexDirection: 'row',
    alignContent : 'center'
  },
  nameNew :{
    marginStart : 14,
    fontSize:responsiveScreenFontSize(2.4),
    color:"#ffffff", 
    fontFamily:'EBGaramond-Bold'
  },
  avatar: {
    marginStart:10,
    width: responsiveWidth(28),
    height:  responsiveWidth(28),
    borderRadius: 63,
    borderWidth: 1,
    borderColor: "#999",
    marginBottom:10,
  }, 
})
