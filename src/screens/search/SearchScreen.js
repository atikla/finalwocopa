import React, {useState,  useRef , useEffect}  from 'react';
import { Text, StyleSheet , View ,ScrollView  , Image, BackHandler, ToastAndroid,NativeModules,Platform,Alert} from 'react-native';  
import '../../../utils/global.js'; 
import { theme } from '../../../utils/theme.js';  
import { Button } from 'react-native-elements';  
import { responsiveHeight,  responsiveScreenFontSize  } from "react-native-responsive-dimensions";
import TitlesSection from '../../../components/Search/TitlesSection'
import GoalsSection from '../../../components/Search/GoalsSection'
import InterestsSection from '../../../components/Search/InterestsSection'
import LocationsSection from '../../../components/Search/LocationsSection'
import { Icon } from 'react-native-elements';
import { useHistory } from 'react-router-native'; 


const {StatusBarManager} = NativeModules
const statusBarHegiht = StatusBarManager.HEIGHT

const  SearchScreen = props => {
 
    const refGoals = useRef(null); 
    const [favTitles, setFavTitles] = useState([]);
    const [favInterests, setFavInterests] = useState([]);
    const [favlocation, setFavLocation] = useState([]); 
    const [favGoals, setFavGoals] = useState([]); 
    let history = useHistory();
   

    useEffect(() =>{  
        BackHandler.addEventListener("hardwareBackPress", backButtonHandler);

        return () => {
        BackHandler.removeEventListener("hardwareBackPress", backButtonHandler);
        };
    }, [backButtonHandler]);


    const backButtonHandler = () => {
        history.goBack()
        return true;
      } 
    
    const _goAdvancedSearch = () =>{
        // props.navigation.navigate('AdvanceSearchScreen')
    }


    const onButtonClick2 = () => {   
        let goals = favGoals ? favGoals : ''
        let titles = favTitles? favTitles: "";
        let interests = favInterests? favInterests : "";
        let Locations = favlocation? favlocation : "";  
        if (interests.length >0 || goals.length >0 || titles.length >0 || Locations.length >0  ){  
            history.push({pathname : '/Main/Search/Result',
            state: {
                goals : goals.toString(),
                titles : titles.toString(),
                interests : interests.toString(),
                Locations : Locations.toString(), 
                }
            })     
        } 
        else {
            if(Platform.OS ==='android'){
                ToastAndroid.show('Enter One Value At Least', ToastAndroid.SHORT);  
            }   
            else{
                Alert.alert('Enter One Value At Least')
            }      
        }
    };

    return (
       
        <View  style = {{width:'100%',height:'100%', backgroundColor:'#fff',paddingVertical :responsiveHeight(1) ,paddingHorizontal:responsiveHeight(3) ,   paddingTop:Platform.OS ==='ios' ? statusBarHegiht : 0 }}>
 
            <View style={{flexDirection:'row',justifyContent:'space-between' ,backgroundColor:'transparent',width:'100%',marginTop:10,zIndex:0}}>
             
                <Icon name="arrow-back" type ={'material'} containerStyle={{alignSelf:'flex-start',   }} size={responsiveScreenFontSize(4)} color={theme.colors.surface} onPress={()=>  history.goBack() }  />
               
   
                <Text style ={{alignSelf:'center', fontSize:responsiveScreenFontSize(2.5),fontFamily:'EBGaramond-SemiBold',color:theme.colors.surface,marginLeft:-10}}>Search Users</Text> 
    
                 <Image
                   resizeMode={'contain'}
                   style={styles.stretch}
                   source={null} 
                   tintColor={ '#1D253C'}
                 /> 
           </View>

            <ScrollView bounces={false} showsVerticalScrollIndicator={false} style = {styles.container}>  
                
                
                <TitlesSection
                    favTitles={favTitles}
                    setFavTitles={setFavTitles}
                />

                <GoalsSection
                    favGoals={favGoals}
                    setFavGoals={setFavGoals}
                />

                <InterestsSection
                    favInterests={favInterests}
                    setFavInterests={setFavInterests}
                />

                <LocationsSection
                    favlocation={favlocation}
                    setFavLocation={setFavLocation}
                /> 
 
                <Button title = "Search Now" buttonStyle={{backgroundColor: theme.colors.surface ,width : '90%',alignSelf:'center' ,height :responsiveHeight(6) ,marginBottom:responsiveHeight(2),borderTopRightRadius:30, borderBottomLeftRadius:30,paddingBottom:10 }}   titleStyle ={{color:"#fff"}}     onPress ={ onButtonClick2 }  >Start Now</Button>
                { global.is_pro ==1 &&
                    <Button title = "Advanced Search" buttonStyle={{backgroundColor:'#bf4f22' ,width : '90%',alignSelf:'center' ,height :responsiveHeight(6) ,marginBottom:responsiveHeight(2),borderTopRightRadius:30, borderBottomLeftRadius:30,paddingBottom:10 }}   titleStyle ={{color:"#fff"}}     onPress ={ _goAdvancedSearch }  >Advanced Search</Button>
                }
 
            </ScrollView>

           
        </View>

  );
};

const styles = StyleSheet.create({
    container: { 
        paddingHorizontal:responsiveHeight(1) ,
        flex : 0.9,
        backgroundColor:'#fff',  
        marginBottom :responsiveHeight(1) , 
      },  
      divider:{
        alignSelf: 'stretch',
        borderBottomColor: 'rgba(0, 113, 183,0.2)',
        borderBottomWidth: 1,
        marginTop : 10,
        marginBottom : 2,
        marginVertical:responsiveHeight(2), 
      },
        
}) 

export default SearchScreen